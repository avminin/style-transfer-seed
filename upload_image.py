import requests

filename_to_upload = 'images/lenna.jpg'

upload_details = {"url":"https://storage.yandexcloud.net/upload-bucket-hw7-my","fields":{"key":"338.jpg","x-amz-algorithm":"AWS4-HMAC-SHA256","x-amz-credential":"glLNzIBePd-SS_Z9Sxik/20210327/ru-central1/s3/aws4_request","x-amz-date":"20210327T141415Z","policy":"eyJleHBpcmF0aW9uIjogIjIwMjEtMDMtMjdUMTU6MTQ6MTVaIiwgImNvbmRpdGlvbnMiOiBbeyJidWNrZXQiOiAidXBsb2FkLWJ1Y2tldC1odzctbXkifSwgeyJrZXkiOiAiMzM4LmpwZyJ9LCB7IngtYW16LWFsZ29yaXRobSI6ICJBV1M0LUhNQUMtU0hBMjU2In0sIHsieC1hbXotY3JlZGVudGlhbCI6ICJnbExOeklCZVBkLVNTX1o5U3hpay8yMDIxMDMyNy9ydS1jZW50cmFsMS9zMy9hd3M0X3JlcXVlc3QifSwgeyJ4LWFtei1kYXRlIjogIjIwMjEwMzI3VDE0MTQxNVoifV19","x-amz-signature":"274f68729a324d75c6ea333c3c6b5e3e30650988cd98cb7e50ed227283861a91"}}

with open(filename_to_upload, 'rb') as file_to_upload:
    files = {'file': (filename_to_upload, file_to_upload)}
    upload_response = requests.post(upload_details['url'], data=upload_details['fields'], files=files)

print(f"Upload response: {upload_response.status_code}")